﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;

namespace DiscussionCaptureAPI
{

    class DiscussionCaptureAPIAccessor
    {
        private string token = null;
        private string clientId = "d17799d18623364dbde39e552e17ac9b3d4daa298b6513eca1f0d4abc8c1afd0";
        /// <summary>
        /// トークン取得
        /// </summary>
        /// <returns>Jsonトークン</returns>
        private async Task<string> GetTokenAccess()
        {

            var Httpclient = new HttpClient();
            var url = "https://demo.discussioncapture.com/api/token";
            var clientSecretKey = "4f95399a3c5cd82d14283eddb22013cd40d11b3fbfa2052f1ea25fe06c1efe5b";

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("X-Client-Id", clientId);
            request.Headers.Add("X-Client-Secret", clientSecretKey);

            var response = await Httpclient.SendAsync(request);
            return await response.Content.ReadAsStringAsync();

        }
        public void GetToken()
        {
            var result = GetTokenAccess().Result;
            var resultJson = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
            token = resultJson["token"];
            Console.WriteLine(token);
        }


        /// <summary>
        /// セッションの作成
        /// </summary>
        /// <returns>各種セッション情報</returns>
        private async Task<string> CreateSessionAccess()
        {

            var Httpclient = new HttpClient();
            var url = "https://demo.discussioncapture.com/api/sessions";
            //var clientId = "d17799d18623364dbde39e552e17ac9b3d4daa298b6513eca1f0d4abc8c1afd0";

            var keywordListId = 2;
            var byod = "true";
            var features = "true";
            var doa = "false";

            //var sessionInfoJson = $"{{\"name\": \"Session Name2\",\"keywordListId\": 2,\"byod\": true,\"features\": true,\"doa\": false}}";
            //var sessionInfoJson = $"{{\"name\": \"Session Name2\", \"keywordListId\": {keywordListId}, \"byod\": {byod.ToString().ToLower()}, \"features\": {features.ToString().ToLower()}, \"doa\": {doa.ToString().ToLower()}}}";
            var sessionInfoJson = $"{{\"name\": \"sessionName\", \"keywordListId\": {keywordListId}, \"byod\": {byod}, \"features\": {features}, \"doa\": {doa}}}";

            Console.WriteLine(sessionInfoJson);

            var content = new StringContent(sessionInfoJson, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, url);

            request.Headers.Add("X-Client-Id", clientId);
            request.Headers.Add("X-Client-Token", token);
            request.Content = content;

            var response = await Httpclient.SendAsync(request);
            return await response.Content.ReadAsStringAsync();

        }
        public string CreateSession()
        {
            if (token == null) { return null; }
            return CreateSessionAccess().Result;
        }

        /// <summary>
        /// デバイスの登録
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>

        private async Task<string> CreateSessionDeviceAccess(string session_id)
        {

            var Httpclient = new HttpClient();
            var url = $"https://demo.discussioncapture.com/api/sessions/{session_id}/devices";

            var deviceInfoJson = $"{{\"name\": \"Device name 1\"}}";

            Console.WriteLine(deviceInfoJson);

            var content = new StringContent(deviceInfoJson, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, url);

            request.Headers.Add("X-Client-Id", clientId);
            request.Headers.Add("X-Client-Token", token);
            request.Content = content;

            var response = await Httpclient.SendAsync(request);
            return await response.Content.ReadAsStringAsync();

        }
        public string CreateSessionDevice(string session_id)
        {
            if (token == null) { return null; }
            return CreateSessionDeviceAccess(session_id).Result;
        }
    }
}

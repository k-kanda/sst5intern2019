﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;

namespace DiscussionCaptureAPI
{
    class Program
    {
        public static void Main(string[] args)
        {
            DiscussionCaptureAPIAccessor dcaa = new DiscussionCaptureAPIAccessor();

            Console.WriteLine("Token取得");
            //トークンを取得
            dcaa.GetToken();

            //セッション
            var sessionResult = dcaa.CreateSession();
            Console.WriteLine(sessionResult);

            if (sessionResult == null) {
                Console.WriteLine("セッションの作成に失敗しました。");
            }

            //結果をオブジェクト化
            var sessionResultJson = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(sessionResult);

            //Console.WriteLine(sessionResultJson["session"]["id"].ToString());


            //デバイスの登録　引数（セッションID）
            var deviceRegistResult = dcaa.CreateSessionDevice(sessionResultJson["session"]["id"].ToString());
            if (deviceRegistResult == null)
            {
                Console.WriteLine("デバイスに失敗しました。");
            }
            Console.WriteLine(deviceRegistResult);




            // 重い処理をした続きの処理
            //Console.WriteLine(resultJson);
            //Console.WriteLine(sessionResult);

            System.Console.ReadLine();
            
        }


    }
}
